#[macro_use]
extern crate structopt;

mod cli;

use std::{
    fs::File,
    path::PathBuf,
    io,
};

use cli::Opts;

use parallel_getter::ParallelGetter;
use snafu::{
    Snafu,
    ResultExt,
    ensure,
};
use structopt::StructOpt;

#[derive(Debug, Snafu)]
enum Error {
    #[snafu(display("File `{}` already exists", filename.display()))]
    PathAlreadyExists { filename: PathBuf },
    #[snafu(display("Failed to execute request: {}", source))]
    RequestError { source: io::Error }
}

type Result<T, E = Error> = std::result::Result<T, E>;

fn run() -> Result<()> {
    let opts = Opts::from_args();
    let filename = opts.output();
    ensure!(!(filename.is_dir() || filename.is_file()), PathAlreadyExists { filename });
    if opts.log_level() > 0 {
        eprintln!("Will download from `{}` to `{}` with {} threads", opts.url(), opts.output().display(), opts.threads());
    }
    let mut output = File::create(filename)
        .expect("could not create output file");
    let getter = ParallelGetter::new(opts.url().as_ref(), &mut output)
        .threads(opts.threads())
        .callback(1000, Box::new(|p, t| {
            println!(
                "{} of {} MiB downloaded",
                p / 1024 / 1024,
                t / 1024 / 1024);
        }));
    getter.get().context(RequestError)?;
    println!("finished!");
    Ok(())
}

fn main() {
    if let Err(err) = run() {
        eprintln!("got an error: {}", err);
    }
}
