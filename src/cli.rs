use std::{
    path::PathBuf,
};

use url::Url;

use structopt::clap;

#[derive(StructOpt, Debug, Hash, Clone)]
#[structopt(rename_all = "kebab-case", raw(
    setting = "clap::AppSettings::ColoredHelp",
))]
pub struct Opts {
    /// The path of the destination of the download.
    ///
    /// If not specified, it will use the remote name of the file from the url. If the url is
    /// https://example.com/img.jpg, the output will give img.jpg. If there is no extension in the
    /// remote name, an `.html` extension will be added. If no remote name can be parse, like for
    /// https://example.com, the output will be written to `index.html`.
    #[structopt(parse(from_os_str), short, long)]
    output: Option<PathBuf>,

    /// The URL of the file to download.
    url: Url,

    /// The number of threads to use for the client.
    #[structopt(short, long)]
    threads: Option<usize>,

    /// Level of verbosity, repeat to increase log level.
    ///
    /// 0 is error and warnings, 1 is info, 2 is debug, 3 is trace.
    #[structopt(short, parse(from_occurrences))]
    verbosity: u32,
}

impl Opts {
    pub fn url(&self) -> &Url {
        &self.url
    }

    pub fn output(&self) -> PathBuf {
        self.output.clone().unwrap_or_else(|| {
            let path = self.url.path_segments()
                .and_then(Iterator::last)
                .filter(|&p| p != "")
                .map(|p| if let Some(_) = p.find('.') {
                    p.to_owned()
                } else { p.to_owned() + ".html" })
                .unwrap_or_else(|| "index.html".to_owned());
            PathBuf::from(path)
        })
    }

    pub fn threads(&self) -> usize {
        self.threads.unwrap_or_else(num_cpus::get)
    }

    pub fn log_level(&self) -> u32 {
        self.verbosity
    }
}
